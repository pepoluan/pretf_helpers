# CHANGELOG


### 0.4.0

So many changes, MAJOR ones, we jump two minor versions...

 * `_utils.py` is now `utils.py`
 * Add `CURRENT_MY_PUBLIC_IP` templating variable
 * Add `CURRENT_COMMIT_ID` templating variable
 * Enforce Template Decoding on load of config file
 * No longer enforces some default tags (i.e., the `Terraform*` tags)
 * A complete rewrite of `_SimpleBlock`, and name change to `_BaseBlock`
 * Make `_CompoundResource` inherits from `Resource` instead of `_SimpleBlock`/`_BaseBlock`
 * Implement "meta-arguments" as properties
   * We are now separating between config items that actually impacts the provider
     with config items that only impact Terraform's processing flow
 * `DictAble` is removed. Subclasses of `DictAble` now inherits from
   `dict` directly, only with a custom `__init__` to enforce mandatory
   args 
 * Classes that begin with `AWS_*` lose that prefix
 * Implement value validation for `_LoadBalancerListeners` and `_SecurityGroupRules`
 * Extract classes from the overburdened `Config` class, to enforce "separation of concerns"
   * Implement `RemoteData` class for `terraform_remote_state` -- this removes `Config.remote`

### 0.2.8

 * Add (optional) non-terminating connection testing logic
 * Move 'generic' functions to `_utils.py`. These functions should
   never be imported anyways.
 * Add more `Terraform*` tags 
 * Add some git intelligence
 * Add pre-made custom workflows
 * Add a very simple template decoding engine

### 0.2.6

 * Apply MPL-2.0 to all source files
 * ... and a lot of internal logic / changes I forgot to record, sorry :-(


### 0.2.0+202007081141

 * Initial Public Release
