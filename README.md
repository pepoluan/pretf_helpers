# Helper Classes for `pretf`

## Description

This is a set of functions and classes to make using [`pretf`][pretf] easier and "more Pythonic".

## Installation

```bash
pip install pretf-helpers
```

### Documentation

Currently provided on [this project's wiki pages][wiki]

It's still a work in progress, I'll populate it with more info and docs in the coming days.

## License

This package is licensed under the [**MPL 2.0** license](https://www.mozilla.org/en-US/MPL/2.0/FAQ/).

Do note that this means (my interpretation):

 * Only files explicitly part of this package must be kept open source.
 * If you add _your own_ files to this package, you are NOT required to open source those files.
 * If you use this package in your software, you are NOT required to open source your software.

Full license text is available in the `LICENSE` file.

## Attributions

The `pretf` package is (c) 2019 Raymond Butcher

[pretf]: https://github.com/raymondbutcher/pretf/
[wiki]: https://gitlab.com/pepoluan/pretf_helpers/-/wikis/home
