# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from abc import ABCMeta, abstractmethod
from collections.abc import Iterable
from pathlib import Path
from os import PathLike
from io import StringIO

from typing import Optional, Union, Callable, Dict, List, Iterator, NamedTuple

# noinspection PyProtectedMember
from pretf.api import block, Block as pretfBlockClass

# noinspection PyProtectedMember
from pretf.blocks import Interpolated

import pretf_helpers._log as log


# region ##### Exceptions
from pretf_helpers.utils import (
    _test_connection,
    TemplateDecoder,
    AttribDict,
    SingletonByArgs,
)


class LabelConflict(RuntimeError):
    """Raised when an exact label has been defined in the same context"""

    pass


# endregion


# region ##### Helper functions & classes
PathEquiv = Union[str, Path, PathLike]
"""Workaround for PyCharm unable to recognize that Path is part of os.PathLike"""


def unwrap(s) -> str:
    """Removes the `${` and `}` wrappers around the returned identifier."""
    if not isinstance(s, str):
        s = str(s)
    if not s.startswith("${"):
        return s
    if not s.endswith("}"):
        return s
    return s[2:-1]


class InitScript:
    """
    Generates a uniform init-script to be used in an instance's first boot (e.g., AWS' "UserData" for
    EC2 instances.
    """

    def __init__(
        self,
        hostname: str = None,
        users_and_keys: Dict[str, List[str]] = None,
        interpreter: str = "/bin/bash",
    ):
        self.hostname = hostname
        self.users_and_keys = users_and_keys
        self.interpreter = interpreter

    def generate(self) -> str:
        with StringIO() as sio:
            sio.write(f"#!{self.interpreter}\n")
            sio.write(f"sudo hostnamectl set-hostname {self.hostname}\n")
            for user, keylist in self.users_and_keys.items():
                dotssh_dir = f"/home/{user}/.ssh"
                authkeys_file = f"{dotssh_dir}/authorized_keys"
                for key in keylist:
                    sio.write(f'printf "\\n%s\\n" "{key}" ')
                    sio.write(f">> {authkeys_file}\n")
                sio.write(f"chown {user}:{user} {authkeys_file}\n")
                sio.write(f"chmod -R 0600 {dotssh_dir} {dotssh_dir}/*\n")
            return sio.getvalue()


# endregion


# region ##### (Abstract) 'Private' Base Classes
class _BaseBlock(Iterable, metaclass=ABCMeta):
    """
    Prototypes a simple block that does not have attached blocks.
    """

    def __init__(self, block_type: str, *labels: str):
        self._type = block_type
        self._labels = labels
        self._body = {}

    @property
    def _block(self) -> pretfBlockClass:
        return block(self._type, *self._labels, self._body)

    # region # Meta-arguments
    @property
    def depends_on(self):
        return self._body.get("depends_on")

    @depends_on.setter
    def depends_on(self, blocks: "Union[_BaseBlock, Iterable[_BaseBlock]]"):
        """
        Sets the `depends_on` meta-arg
        :param blocks: Either an instance of _BaseBlock or a list of _BaseBlock instances
        """
        if isinstance(blocks, _BaseBlock):
            blocks = [blocks]
        self._body["depends_on"] = [unwrap(b._block) for b in blocks]

    @property
    def provider(self) -> str:
        return self._body.get("provider")

    @provider.setter
    def provider(self, provider: Union["_Provider", "_ProviderAlias", str]):
        if isinstance(provider, _Provider):
            self._body["provider"] = provider.reference
            return

        if isinstance(provider, str):
            provider = _ProviderAlias.parse(provider)

        if not isinstance(provider, _ProviderAlias):
            raise TypeError(f"Cannot process value of type '{type(provider)}'")

        if provider not in _Provider:
            raise KeyError(f"Provider '{provider}' is not defined!")

        self._body["provider"] = str(provider)

    @property
    def lifecycle(self) -> "LifeCycle":
        return self._body.get("lifecycle")

    @lifecycle.setter
    def lifecycle(self, value: dict):
        if value:
            self._body["lifecycle"] = LifeCycle(**value)

    # endregion

    def __getattr__(self, item) -> Interpolated:
        return getattr(self._block, item)

    def __iter__(self) -> Iterator[pretfBlockClass]:
        yield self._block

    # We use @abstractmethod here to force subclasses to implement the __call__ method,
    # because __call__ does not get inherited automatically.
    @abstractmethod
    def __call__(self, **kwargs) -> "_BaseBlock":
        self._body.update(kwargs)
        return self


# noinspection PyNestedDecorators
class _Backend(Iterable, metaclass=ABCMeta):

    # These two ... 'constructs' are to implement "Abstract Class Properties"
    # Ref: https://stackoverflow.com/a/53417582/149900

    # noinspection PyPropertyDefinition
    @property
    @classmethod
    @abstractmethod
    def ConfigAttribRemaps(cls) -> dict:
        raise NotImplementedError  # pragma: no cover

    def __init__(
        self, test_connection: bool = False, renderer: Callable = None, **kwargs
    ):
        if test_connection:
            _test_connection()
        self._cfg = kwargs
        self._renderer = renderer

    @property
    @abstractmethod
    def type(self) -> str:
        raise NotImplementedError  # pragma: no cover

    @property
    @abstractmethod
    def remote_config(self) -> dict:
        raise NotImplementedError  # pragma: no cover

    def __iter__(self) -> Iterator[pretfBlockClass]:
        yield self._renderer(**self._cfg)

    @classmethod
    def from_config_(cls, config) -> "_Backend":
        kwargs = dict(config)
        # noinspection PyUnresolvedReferences
        for orig, dest in cls.ConfigAttribRemaps.items():
            if orig not in kwargs:
                continue
            kwargs[dest] = kwargs.pop(orig)
        return cls(**kwargs)


class _ProviderAlias(NamedTuple):
    provider: str
    alias: Optional[str] = None

    def __str__(self):
        if self.alias is None:
            return self.provider
        return self.provider + "." + self.alias

    @classmethod
    def parse(cls, s: str) -> "_ProviderAlias":
        provider, alias = s.split(".") if "." in s else (s, None)
        # We'd love to use `cls` here instead of `_ProviderAlias`, but unfortunately
        # PyCharm still uses NamedTuple's signature to validate. Plus, there does not
        # seem to be any PyCharm directive to ignore the next line.
        return _ProviderAlias(provider=provider, alias=alias)


class _ProviderMeta(ABCMeta):
    AliasedProviders: Dict[_ProviderAlias, "_Provider"] = {}

    def __init__(cls, *args, **kwargs):
        super().__init__(*args, **kwargs)
        cls.AliasedProviders = cls.__class__.AliasedProviders

    def __contains__(self, item: Union[_ProviderAlias, str]):
        if isinstance(item, str):
            item = _ProviderAlias.parse(item)
        if isinstance(item, _ProviderAlias):
            return item in self.__class__.AliasedProviders
        raise TypeError("item must be either str or _ProviderAlias")


class _Provider(metaclass=_ProviderMeta):
    def __init__(
        self,
        renderer: Callable,
        provider: str,
        alias: Optional[str] = None,
        test_connection: bool = True,
    ):
        super().__init__()

        self.id = _ProviderAlias(provider, alias)
        if self.id in self.__class__.AliasedProviders:
            raise KeyError(f"Attempt to redefine aliased provider '{self.id}'")
        self.__class__.AliasedProviders[self.id] = self

        if test_connection:
            _test_connection()

        self._body = {}
        if alias:
            self._body["alias"] = alias
        self._renderer: Callable = renderer

    @abstractmethod
    def __call__(self, **kwargs) -> "_Provider":
        self._body.update(kwargs)
        return self

    def __iter__(self) -> Iterator[block]:
        yield self._renderer(**self._body)

    #
    # def __contains__(self, item) -> bool:
    #     if isinstance(item, str):
    #         item = _ProviderAlias.parse(item)
    #     if isinstance(item, _ProviderAlias):
    #         return item in _Provider.AliasedProviders
    #     raise TypeError("item must be either str or _ProviderAlias")

    @property
    def reference(self) -> str:
        return str(self.id)


class LifeCycle(dict):
    def __init__(
        self,
        *,
        create_before_destroy: bool = None,
        prevent_destroy: bool = None,
        ignore_changes: "Union[str, Iterable[str]]" = None,
    ):
        super().__init__()
        self._validkey: bool = False
        if create_before_destroy is not None:
            self.create_before_destroy = create_before_destroy
        if prevent_destroy is not None:
            self.create_before_destroy = create_before_destroy
        if ignore_changes is not None:
            self.ignore_changes = ignore_changes

    def __setitem__(self, key, value):
        if not self._validkey:
            raise KeyError(f"Key '{key}' not allowed")
        super().__setitem__(key, value)
        self._validkey = False

    @property
    def create_before_destroy(self) -> bool:
        return self["create_before_destroy"]

    @create_before_destroy.setter
    def create_before_destroy(self, value):
        self._validkey = True
        self["create_before_destroy"] = bool(value)

    @property
    def prevent_destroy(self) -> bool:
        return self["prevent_destroy"]

    @prevent_destroy.setter
    def prevent_destroy(self, value):
        self._validkey = True
        self["prevent_destroy"] = bool(value)

    @property
    def ignore_changes(self) -> list:
        return self["ignore_changes"]

    @ignore_changes.setter
    def ignore_changes(self, value: "Union[str, Iterable[str]]"):
        self._validkey = True
        if isinstance(value, str):
            self["ignore_changes"] = [value]
            return
        if not isinstance(value, Iterable):
            raise TypeError("ignore_changes must be either str or Iterable[str]")
        if not all(isinstance(v, str) for v in value):
            raise TypeError("ignore_changes must only contain string values")
        self["ignore_changes"] = list(value)


class _Collector(metaclass=ABCMeta):
    # Enforce abstract base class
    @abstractmethod
    def __init__(self, renderer: Callable):
        """
        Implements a collection -- with uniqueness -- whose members/elements can be restricted and/or preprocessed
        before getting collected.

        :param renderer: a function that accepts 1 arg "label" and returns another function that can build the
        configuration body if given the kwargs.
        Usually implemented as `partial(BlockGeneratingClass, "block_type")`
        """
        self._coll: Dict[str, dict] = {}

        if not isinstance(renderer, Callable):
            raise TypeError("renderer must be callable")
        self._renderer: Callable[[str], Callable] = renderer
        """
        self._renderer contains a function that accepts 1 arg "label"; this function must itself return
        a function that will be called with the actual body/config. Usually implemented like this:
        `partial(BlockGeneratingClass, "block_type")`
        """

    # noinspection PyMethodMayBeStatic
    def _element(self, value_dict: dict) -> dict:
        """Can be overridden to implement preprocessing/validation"""
        return value_dict

    def get_raw(self, item) -> dict:
        return self._coll[item]

    def __getitem__(self, item) -> _BaseBlock:
        return self._renderer(item)(**self._coll[item])

    def __setitem__(self, key, value: dict):
        if key in self._coll:
            raise LabelConflict(f"Attempting to redefine element '{key}'")
        if not isinstance(value, dict):
            raise ValueError(f"Value must be dict or a dict subclass")
        self._coll[key] = self._element(value)

    def __iter__(self) -> Iterator[block]:
        yield from (
            self._renderer(label)(**element) for label, element in self._coll.items()
        )


# endregion


class Resource(_BaseBlock):
    def __init__(self, res_type: str, label: str):
        super().__init__("resource", res_type, label)

    # Need to implement this or else subclasses of this won't be able to
    # do super().__call__()
    def __call__(self, **kwargs) -> "Resource":
        return super().__call__(**kwargs)


class _CompoundResource(Resource, metaclass=ABCMeta):
    """Prototypes a resource that has attached sub-resources."""

    def __init__(self, label1: str, label2: str):
        super().__init__(label1, label2)
        self._subres: Optional["_Collector"] = None

    # We use @abstractmethod here to force subclasses to implement the __call__ method,
    # because __call__ does not get inherited automatically.
    @abstractmethod
    def __call__(self, **kwargs) -> "_CompoundResource":
        return super().__call__(**kwargs)

    def __getitem__(self, item) -> _BaseBlock:
        return self._subres[item]

    def __setitem__(self, key, value):
        self._subres[key] = value

    def __iter__(self) -> Iterator[block]:
        yield self._block
        yield from self._subres


class Data(_BaseBlock):
    def __init__(self, data_type: str, label: str):
        super().__init__("data", data_type, label)

    # Need to implement this or else subclasses of this won't be able to
    # do super().__call__()
    def __call__(self, **kwargs) -> "Data":
        return super().__call__(**kwargs)


# Need to inherit ABCMeta or will cause metaclass conflict in RemoteData
# Ref: https://stackoverflow.com/a/57351066/149900
class _RemoteMeta(ABCMeta):
    _Remotes: Dict[str, "RemoteData"] = {}

    def __init__(cls, *args, **kwargs):
        super().__init__(*args, **kwargs)
        cls.__class__._Remotes = {}

    def __call__(cls, label: str, *args, **kwargs) -> "RemoteData":
        remotes = cls.__class__._Remotes
        if label not in remotes:
            remotes[label] = super().__call__(label, *args, **kwargs)
        return remotes[label]

    def __getitem__(self, label) -> "RemoteData":
        remotes = self.__class__._Remotes
        if label not in remotes:
            remotes[label] = self(label)
        return remotes[label]

    def __getattr__(self, attr) -> "RemoteData":
        return self[attr]


class RemoteData(Data, metaclass=_RemoteMeta):
    """
    Implements the `terraform_remote_state` block.
    """

    def __init__(self, label: str):
        super().__init__("terraform_remote_state", label)

    def __call__(self, backend: Union[str, _Backend], **kwargs) -> "RemoteData":
        """

        :param backend: Either a str containing the backend type, or a _Backend object
        :param kwargs: If backend is str, contains the backend configuration. Must be empty if backend is a
        _Backend object
        :return: This object
        """
        if isinstance(backend, _Backend):
            if kwargs:
                raise ValueError(
                    "If _Backend object is provided, kwargs must not be specified"
                )
            kwargs = backend.remote_config
            backend = backend.type
        super().__call__(backend=backend, config=kwargs)
        return self

    def __getitem__(self, item) -> Interpolated:
        """Gets the item/attribute from the `outputs` arg of the underlying block()"""
        return getattr(self._outputs, item)

    def __getattr__(self, item) -> Interpolated:
        """Gets the item/attribute from the `outputs` arg of the underlying block()"""
        return getattr(self._outputs, item)

    @property
    def _outputs(self) -> Interpolated:
        return self._block.outputs


# region ##### Config system
class _ConfigClassMeta(SingletonByArgs):
    def __call__(
        cls,
        *,
        config_file: str = "config.yaml",
        secrets_file: str = "secrets.yaml",
        default_tags_key: str = "default_tags",
    ):
        return super().__call__(
            config_file=config_file,
            secrets_file=secrets_file,
            default_tags_key=default_tags_key
        )


class ConfigClass(metaclass=_ConfigClassMeta):
    def __init__(
        self,
        *,
        config_file: str = "config.yaml",
        secrets_file: str = "secrets.yaml",
        default_tags_key: str = "default_tags",
    ):
        _cwd = Path.cwd()

        self._config_file: PathEquiv = _cwd / config_file
        self.__cfgd: Optional[AttribDict] = None

        # Defer reading secrets file until actually needed,
        # because it is possible that we are not using it.
        self._secrets_file: PathEquiv = _cwd / secrets_file
        self._secrets = None

        self.__default_tags_key: str = default_tags_key
        self._default_tags: Optional[dict] = None

    @property
    def _cfg(self) -> AttribDict:
        if self.__cfgd is None:
            try:
                self.__cfgd = AttribDict.from_yaml_(
                    TemplateDecoder(error_on_unknown=True).render_file(
                        self._config_file
                    )
                )
            except FileNotFoundError:
                raise log.bad(f"Cannot find config file '{self._config_file}'")
            except ValueError:
                raise log.bad(
                    f"Config file '{self._config_file}' should have been a dict, but it's not"
                )
        return self.__cfgd

    def __getattr__(self, item: str):
        return getattr(self._cfg, item)

    def __getitem__(self, item):
        return self._cfg[item]

    def tags(self, **kwargs) -> dict:
        """
        Generates a dict for tags, derived (copied) from self._default_tags.
        Any non-None kwargs will update (add/replace) the derived dict.
        Any None kwargs will DELETE the same key (if exists) from the derived dict.

        :param kwargs: (Optional) Tags to add/replace/delete
        :return: Final set of tags
        """
        if self._default_tags is None:
            self._default_tags = self._cfg.get(self.__default_tags_key, {})
        d = self._default_tags.copy()
        for k, v in kwargs.items():
            if v is None and k in d:
                del d[k]
            else:
                d[k] = v
        return d

    @property
    def SECRETS(self) -> AttribDict:
        if self._secrets is None:
            try:
                self._secrets = AttribDict.from_yaml_(self._secrets_file)
            except FileNotFoundError:
                raise log.bad(f"Cannot find secrets file '{self._secrets_file}' in cwd")
            except ValueError:
                raise log.bad(
                    f"Secrets file '{self._secrets_file}' should have been a dict, but it's not"
                )
        return self._secrets

    @SECRETS.setter
    def SECRETS(self, value: PathEquiv):
        self._secrets_file = value
        self._secrets = None

    def from_item_(self, config_item) -> AttribDict:
        if isinstance(config_item, dict):
            config_item = AttribDict.recursively_from_dict_(config_item)
        elif isinstance(config_item, str):
            config_item = self[config_item]
        else:
            raise TypeError("config_item must be one of (AttribDict, dict, str)")
        return config_item


Config = ConfigClass()

# endregion

# End of pretf_helpers/__init__.py
