# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from abc import ABCMeta, abstractmethod
from functools import partial
from typing import Union, Optional, Dict, List, Sequence

from pretf.aws import provider_aws, terraform_backend_s3

from pretf_helpers import (
    Resource,
    Config,
    Data,
    _Backend,
    _Collector,
    _CompoundResource,
    _Provider,
)
from pretf_helpers.utils import AttribDict


# region ### AWS Helper Classes
class _AWS_HasRegion(metaclass=ABCMeta):  # pragma: no cover
    @property
    @abstractmethod
    def region(self):
        raise NotImplementedError

    @region.setter
    @abstractmethod
    def region(self, value):
        raise NotImplementedError


# endregion


# region ### AWS Provider & Backend
class Provider(_Provider, _AWS_HasRegion):
    DefaultRegion: str = None

    def __init__(self, alias: str = None, *, version: str, test_connection: bool = True):
        super().__init__(
            renderer=provider_aws,
            provider="aws",
            alias=alias,
            test_connection=test_connection,
        )
        self._version = version

    def __call__(
        self, *, region: Optional[str] = None, **kwargs
    ) -> "Provider":
        """

        :param version:
        :param region: If set to None then uses the class's DefaultRegion
        :param kwargs:
        :return:
        """
        super().__call__(**kwargs)
        self.region = region
        self._body["version"] = self._version
        return self

    @property
    def region(self) -> str:
        return self._body["region"]

    @region.setter
    def region(self, value):
        if value is None:
            if self.DefaultRegion is None:
                raise ValueError("Region not specified but DefaultRegion not set")
            self._body["region"] = self.DefaultRegion
        else:
            self._body["region"] = value


class Backend_S3(_Backend, _AWS_HasRegion):
    ConfigAttribRemaps = {
        "tfstate": "key",
        "state_db": "dynamodb_table",
    }
    DefaultRegion: str = None

    def __init__(self, bucket: str, key: str, region: str = None, **kwargs):
        super().__init__(renderer=terraform_backend_s3, **kwargs)
        self.region = region
        self._cfg["bucket"] = bucket
        self._cfg["key"] = key

    @property
    def type(self) -> str:
        return "s3"

    @property
    def remote_config(self) -> dict:
        return {
            "bucket": self._cfg["bucket"],
            "key": self._cfg["key"],
            "region": self.region,
        }

    @property
    def region(self) -> str:
        return self._cfg.get("region")

    @region.setter
    def region(self, value: str):
        if value is None:
            self._cfg["region"] = self.DefaultRegion
        else:
            self._cfg["region"] = value


# endregion


# region ### RDS Classes
class RDS_Parameter_Group(Resource):
    def __init__(self, label: str):
        super().__init__("aws_db_parameter_group", label)

    def __call__(
        self,
        family: str,
        tags: Optional[Dict[str, str]] = None,
        parameters: Optional[Union[list, dict]] = None,
        parameter: Optional[list] = None,
        **kwargs,
    ) -> "RDS_Parameter_Group":
        if parameter is None:
            parameter = []
        if parameters is not None:
            if isinstance(parameters, dict):
                parameter.extend({"name": k, "value": v} for k, v in parameters.items())
            elif isinstance(parameters, list):
                parameter.extend(parameters)
            else:
                raise TypeError("options must be dict or list")

        tags = tags or {}
        super().__call__(
            family=family, parameter=parameter, tags=Config.tags(**tags), **kwargs
        )
        return self

    def from_config_item_(
        self, config_item: Union[AttribDict, dict, str], tags: Optional[dict] = None
    ) -> "RDS_Parameter_Group":
        config_item = Config.from_item_(config_item)
        tags = tags or {}
        return self(
            name=config_item.name,
            description=config_item.description,
            family=config_item.family,
            parameters=config_item.parameters,
            tags=tags,
        )


class RDS_Option_Group(Resource):
    def __init__(self, label: str):
        super().__init__("aws_db_option_group", label)

    def __call__(
        self,
        engine_name: str,
        major_engine_version: str,
        options: Optional[Union[list, Dict[str, str]]] = None,
        option: Optional[list] = None,
        tags: Optional[Dict[str, str]] = None,
        **kwargs,
    ) -> "RDS_Option_Group":
        if option is None:
            option = []
        if options is not None:
            if isinstance(options, dict):
                for opt_name, opt_setval in options.items():
                    for s_name, s_val in opt_setval.items():
                        option.append(
                            {
                                "option_name": opt_name,
                                "option_settings": {"name": s_name, "value": s_val},
                            }
                        )
            elif isinstance(options, list):
                raise NotImplementedError(
                    "Currently you can only specify options as dict"
                )
            else:
                raise TypeError("options must be dict or list")

        tags = tags or {}
        super().__call__(
            engine_name=engine_name,
            major_engine_version=major_engine_version,
            tags=Config.tags(**tags),
            option=option,
            **kwargs,
        )
        return self

    def from_config_item_(
        self, config_item: Union[AttribDict, dict, str], tags: Optional[dict] = None
    ) -> "RDS_Option_Group":
        config_item = Config.from_item_(config_item)
        tags = tags or {}
        return self(
            name=config_item.name,
            option_group_description=config_item.description,
            engine_name=config_item.engine_name,
            major_engine_version=config_item.major_engine_version,
            options=config_item.get("options"),
            tags=tags,
        )


class RDS_Instance(Resource):
    CloudWatchLogsExports = ["error", "slowquery"]

    def __init__(self, label: str):
        super().__init__("aws_db_instance", label)

    def __call__(
        self,
        config_item,
        parameter_group: RDS_Parameter_Group,
        option_group: RDS_Option_Group,
        subnet_group_name,
        security_group_ids,
        username: str,
        password: str,
        **kwargs,
    ) -> "RDS_Instance":
        cfg = Config.from_item_(config_item)
        deps = [parameter_group.unwrapped__, option_group.unwrapped__]
        return super().__call__(
            depends_on=deps,
            identifier=cfg.identifier,
            multi_az=cfg.multi_az,
            db_subnet_group_name=subnet_group_name,
            vpc_security_group_ids=security_group_ids,
            instance_class=cfg.type,
            allocated_storage=cfg.storage_min_gb,
            max_allocated_storage=cfg.storage_max_gb,
            storage_type=cfg.storage_type,
            engine=cfg.engine,
            engine_version=cfg.engine_version,
            # parameter_group_name=parameter_group.block__.name,
            # option_group_name=option_group.block__.name,
            parameter_group_name=parameter_group.name,
            option_group_name=option_group.name,
            monitoring_interval=cfg.monitoring_interval,
            monitoring_role_arn=cfg.monitoring_role_arn,
            performance_insights_enabled=cfg.performance_insights,
            enabled_cloudwatch_logs_exports=self.CloudWatchLogsExports,
            backup_retention_period=cfg.backup_retention,
            backup_window=cfg.backup_window,
            copy_tags_to_snapshot=True,
            skip_final_snapshot=False,
            username=username,
            password=password,
            tags=Config.tags(Name=cfg.name),
        )


# endregion


# region ### LoadBalancer Classes
class LB_TargetGroup(Resource):
    def __init__(self, label: str):
        super().__init__("aws_lb_target_group", label)

    def __call__(
        self,
        *,
        name: str,
        port: Union[int, str],
        protocol: str,
        vpc_id,
        health_check_ports: Optional[str] = None,
        healthy_threshold: int = 5,
        unhealthy_threshold: int = 2,
    ) -> "LB_TargetGroup":
        kwargs = {
            "name": name,
            "port": port,
            "protocol": protocol,
            "vpc_id": vpc_id,
        }
        if health_check_ports == "disabled":
            kwargs["health_check"] = {"enabled": False}
        else:
            kwargs["health_check"] = {
                "enabled": True,
                "matcher": health_check_ports or "200",
                "healthy_threshold": healthy_threshold,
                "unhealthy_threshold": unhealthy_threshold,
            }
        return super().__call__(**kwargs)


class LoadBalancerListener(dict):
    def __init__(self, port: str, default_action: Union[List[dict], dict], **kwargs):
        super().__init__(port=port, default_action=default_action)
        self.update(kwargs)


class _LoadBalancerListeners(_Collector):
    def __init__(self, lb_arn):
        super().__init__(renderer=partial(Resource, "aws_lb_listener"))
        self.lb_arn = lb_arn

    def _element(self, value_dict: dict) -> dict:
        _listener = LoadBalancerListener(**value_dict)
        _listener["load_balancer_arn"] = self.lb_arn
        return _listener


class LoadBalancer(_CompoundResource):
    def __init__(self, label):
        super().__init__("aws_lb", label)

    def __call__(
        self,
        *,
        name: str,
        internal: bool,
        load_balancer_type: str,
        security_groups: list,
        subnets: list,
        enable_deletion_protection: bool,
        tags: Dict[str, str],
    ) -> "LoadBalancer":
        super().__call__(
            name=name,
            internal=internal,
            load_balancer_type=load_balancer_type,
            security_groups=security_groups,
            subnets=subnets,
            enable_deletion_protection=enable_deletion_protection,
            tags=tags,
        )
        self._subres = _LoadBalancerListeners(lb_arn=self._block.arn)
        return self

    @property
    def listeners(self) -> _LoadBalancerListeners:
        return self._subres


# endregion


# region ### EC2 Classes
class EC2_AMI(Data):
    def __init__(self, label: str):
        super().__init__("aws_ami", label)

    def __call__(
        self,
        *,
        owners: Union[str, Sequence[str]],
        most_recent: bool = True,
        **filters: Union[str, Sequence[str]],
    ) -> "EC2_AMI":
        """
        Actually builds the "Data" block to get the AMI ID. The **filters will be marhalled into a format
        that Terraform expects.

        :param owners: A str containing AWS ID, or list of str containing AWS IDs
        :param most_recent: Whether to find only the most recent AMI
        :param filters: Keyword args representing the filter to be used in the search
        """
        owners: List[str] = [owners] if isinstance(owners, str) else list(owners)
        filter_list: List[Dict[str, Union[str, List[str]]]] = [
            {"name": k, "values": [v] if isinstance(v, str) else list(v)}
            for k, v in filters.items()
        ]
        return super().__call__(
            most_recent=most_recent, owners=owners, filter=filter_list,
        )


# endregion


# region ### SecurityGroup Classes
class SecurityGroupRule(dict):
    """Helper class to ensure that mandatory fields are provided when creating a Security Group Rule."""

    # noinspection PyShadowingBuiltins
    def __init__(
        self,
        *,
        type: str,
        from_port: int,
        to_port: int,
        protocol: Union[str, int],
        description: Optional[str] = None,
        **kwargs,
    ):
        super().__init__(
            type=type, from_port=from_port, to_port=to_port, protocol=protocol
        )
        self.update(kwargs)
        # super().__init__(**kwargs)
        # self.config = dict(
        #     **kwargs, type=type, from_port=from_port, to_port=to_port, protocol=protocol
        # )
        if description is not None:
            self["description"] = description


class _SecurityGroupRules(_Collector):
    def __init__(self, sg_id):
        super().__init__(renderer=partial(Resource, "aws_security_group_rule"))
        self.sg_id = sg_id

    def _element(self, value_dict: dict) -> dict:
        _rule = SecurityGroupRule(**value_dict)
        _rule["security_group_id"] = self.sg_id
        return _rule


class SecurityGroup(_CompoundResource):
    """Implements an AWS Security Group Resource, with subresources containing AWS Security Group Rule Resources"""

    ParentVPC = None

    def __init__(self, label):
        super().__init__("aws_security_group", label)

    def __call__(
        self, *, name, parent_vpc=None, description: str, tags: dict = None
    ) -> "SecurityGroup":
        super().__call__(
            name=name,
            description=description,
            vpc_id=(parent_vpc or self.__class__.ParentVPC).id,
            tags=tags,
        )
        self._subres = _SecurityGroupRules(sg_id=self._block.id)
        return self

    @property
    def rules(self) -> _SecurityGroupRules:
        return self._subres


# endregion


# End of pretf_helpers/aws.py
