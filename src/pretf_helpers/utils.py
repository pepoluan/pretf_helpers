from functools import lru_cache, partial
from socket import timeout as sock_timeout
from datetime import datetime, timezone
from string import Formatter
from subprocess import check_output, CalledProcessError, DEVNULL
from pathlib import Path
from os import PathLike
from urllib.error import HTTPError, URLError
from urllib.request import urlopen
from json import loads as json_loads

# The next is to prevent LookupError: unknown encoding: idna
# Ref: https://stackoverflow.com/a/54800597/149900
# noinspection PyUnresolvedReferences
import encodings.idna

from typing import Dict, Callable, NamedTuple, Optional, Union, Tuple, FrozenSet

from ruamel import yaml as ryaml

from pretf_helpers import _log as log

PathEquiv = Union[str, Path, PathLike]
"""Workaround for PyCharm unable to recognize that Path is part of os.PathLike"""


_GIT_UPSTREAM = "origin"
_DT_FORMAT = "%Y%m%d%H%M%S"
_TESTCONN_URL = "http://www.neverssl.com"


class IPProvider(NamedTuple):
    url: str
    json_key: Optional[str] = None


_IP_PROVIDERS = {
    "httpbin": IPProvider("https://httpbin.org/ip", "origin"),
    "jsonip": IPProvider("https://jsonip.com", "ip"),
    "42pl": IPProvider("https://ip.42.pl/raw"),
    "ipecho": IPProvider("https://ipecho.net/plain"),
    "ipconfig.in": IPProvider("https://ipconfig.in/ip"),
    "ipify": IPProvider("https://api.ipify.org/?format=raw"),
}
PREFERRED_IP_PROVIDER = "ipecho"


class GitError(CalledProcessError):
    pass


def _ceko(cmd, strip: bool = True) -> str:
    if isinstance(cmd, str):
        cmd = cmd.split()
    rslt = check_output(cmd, stderr=DEVNULL).decode()
    return rslt.strip() if strip else rslt


def _gitdirty(warn: bool = True) -> bool:  # pragma: no cover
    try:
        dirty = bool(_ceko("git status --short"))
    except CalledProcessError:
        raise GitError
    if warn and dirty:
        log.warn("WARNING: git repo is dirty! Don't forget to commit!")
    return dirty


@lru_cache(maxsize=1)
def _getgitcommit() -> str:
    try:
        commit_id = _ceko("git rev-parse --short HEAD")
    # If not a git repo, git will return with non-zero code, raising CalledProcessError
    except CalledProcessError:
        return "NotGit"

    if _gitdirty():
        return f"{commit_id} DIRTY"
    else:
        return commit_id


@lru_cache(maxsize=1)
def _getusername() -> str:
    return _ceko("whoami").split("\\")[-1]


@lru_cache(maxsize=1)
def _getreponame() -> str:
    try:
        return (
            _ceko(f"git remote get-url {_GIT_UPSTREAM}".split())
            .split("/")[-1]
            .replace(".git", "")
        )
    except CalledProcessError:
        # If not a git repo, or there is no remote of the specified name,
        # git will return with non-zero code, raising CalledProcessError
        return f"local:{Path.cwd().name}"


@lru_cache(maxsize=1)
def _getmyip(provider_id: str = None) -> str:
    provider_id = provider_id or PREFERRED_IP_PROVIDER
    url, jsonkey = _IP_PROVIDERS[provider_id]
    with urlopen(url) as http:
        data = http.read().decode()

    if jsonkey:
        return json_loads(data)[jsonkey]  # pragma: no cover
    else:
        return data  # pragma: no cover


def _test_connection():
    try:
        with urlopen(_TESTCONN_URL, timeout=3.0) as http:
            _ = http.read()
    except (HTTPError, URLError, sock_timeout):
        log.warn("No internet connection detected!")


def _getdtstamp():
    utc_now = datetime.now(tz=timezone.utc)
    return utc_now.strftime(_DT_FORMAT)


class TemplateDecoder:
    def __init__(self, error_on_unknown: bool = False):
        self._eunk = error_on_unknown
        self.funcs: Dict[str, Callable[[], str]] = {
            "CURRENT_USER": _getusername,
            "CURRENT_REPO": _getreponame,
            "CURRENT_DATETIME": _getdtstamp,
            "CURRENT_MY_PUBLIC_IP": _getmyip,
            "CURRENT_COMMIT_ID": _getgitcommit,
        }

    def _getval(self, name: str) -> str:
        if name in self.funcs:
            return self.funcs[name]()

        if self._eunk:
            raise KeyError(f"Unknown Template Key {{{{{name}}}}}")
        else:
            return "{" + name + "}"

    def keys(self):
        return self.funcs.keys()

    def render(self, template: str) -> str:
        sf = Formatter()
        rd = {vn: self._getval(vn) for _, vn, _, _ in sf.parse(template) if vn}
        return sf.format(template, **rd)

    def render_file(self, file: PathEquiv) -> str:
        with open(file, "rt") as fin:
            data = fin.read()
        return self.render(data)


class AttribDict(dict):
    """
    Implements a dict whose keys can be accessed like attributes.
    IMPORTANT: When invoking attribs with underscores, e.g. "like_this_one", if the key is not found in the
    underlying dict, this class will try looking for a key with the underscores replaced by dashes, "like-this-one".
    If you do not want this behavior, set the `._trydash` property to False after instantiation.
    """
    # TODO: UPDATE THE DOCSTRING!!!
    def __init__(self, *args, _trydash: bool = True, **kwargs):
        super().__init__(*args, **kwargs)
        self._trydash = _trydash

    def __getattr__(self, name: str):
        if name in self:
            return self[name]
        if self._trydash:
            name_dashed = name.replace("_", "-")
            if name_dashed in self:
                return self[name_dashed]
        # All variants have been exhausted, let superclass handle the missing attrib
        return super().__getattribute__(name)

    @staticmethod
    def recursively_from_dict_(d: dict) -> "AttribDict":
        """
        Creates an AttribDict instance from a dict, recursively converting dict values into AttribDict objects.
        IMPORTANT NOTES:
        (1) Non-dict mutable values (e.g., list, set, etc.) will NOT be copied.
        (2) dicts inside mutable values (e.g., as an element of a list) will NOT be converted.

        :param d: dict object from which to create an AttribDict
        :returns: An AttribDict; if d is already an AttribDict, a copy will be created
        """
        if not isinstance(d, dict):
            raise TypeError("Arg must be a dict or subclass of")
        rslt = AttribDict()
        for k, v in d.items():
            if isinstance(v, dict):
                rslt[k] = AttribDict.recursively_from_dict_(v)
            else:
                rslt[k] = v
        return rslt

    @staticmethod
    def from_yaml_(yaml: Union[str, PathEquiv]) -> "AttribDict":
        """
        Create an AttribDict instance from a YAML file (or YAML string), recursively converting dict values
        into AttribDict objects.
        See IMPORTANT NOTES on `recursively_from_dict_` staticmethod.

        :param yaml: EITHER path to the YAML file OR string containing valid YAML
        """
        data = None
        if isinstance(yaml, str):
            try:
                if not Path(yaml).exists():
                    data = ryaml.safe_load(yaml)
            except OSError:
                data = ryaml.safe_load(yaml)
        if data is None:
            with open(yaml, "rt") as fin:
                data = ryaml.safe_load(fin)
        if not isinstance(data, dict):
            raise ValueError(
                f"The yaml arg should have a dict as root object, but it doesn't!"
            )
        return AttribDict.recursively_from_dict_(data)


class SingletonByArgs(type):
    _Instances: Dict[Tuple[type, Tuple, FrozenSet], object] = {}

    def __call__(cls, *args, **kwargs):
        t = cls, args, frozenset(kwargs.items())
        return cls._Instances.setdefault(t, super().__call__(*args, **kwargs))
