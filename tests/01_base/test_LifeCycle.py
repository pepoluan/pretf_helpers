import pytest

# noinspection PyProtectedMember
from pretf_helpers import LifeCycle


# noinspection PyArgumentList
@pytest.mark.parametrize("args", [(True, ), (True, True), (True, True, "test")])
def test_init_fail(argcount_exception, args):
    with argcount_exception:
        LifeCycle(*args)


def test_init_success():
    assert isinstance(LifeCycle(), dict)
    LifeCycle(create_before_destroy=True)
    LifeCycle(prevent_destroy=True)
    LifeCycle(ignore_changes="test")
    LifeCycle(create_before_destroy=True, prevent_destroy=True)
    LifeCycle(create_before_destroy=True, ignore_changes="test")
    LifeCycle(create_before_destroy=True, prevent_destroy=True, ignore_changes="test")


def test_empty():
    assert not LifeCycle().keys()


@pytest.fixture(scope="module")
def lc():
    return LifeCycle()


@pytest.mark.parametrize(
    "attr,value,expected",
    [
        ("create_before_destroy", False, None),
        ("prevent_destroy", False, None),
        ("ignore_changes", "test", ["test"]),
        ("ignore_changes", ["test", "test"], None),
    ],
)
def test_attr_success(lc, attr, value, expected):
    setattr(lc, attr, value)
    assert attr in lc
    assert getattr(lc, attr, None) == (expected or value)
    assert lc[attr] == (expected or value)


@pytest.mark.parametrize("value", [False, 1, ["test", 1]])
def test_ign_err(lc, value):
    with pytest.raises(TypeError):
        setattr(lc, "ignore_changes", value)


def test_invalidkey():
    lc = LifeCycle()
    with pytest.raises(KeyError):
        lc["dummy"] = "any"
