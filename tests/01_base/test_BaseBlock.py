import pytest

import pretf.api
import pretf.blocks

# noinspection PyProtectedMember
from pretf_helpers import _BaseBlock, _Provider, _ProviderAlias, LifeCycle


# noinspection PyAbstractClass
class IncompleteConcreteBaseBlock(_BaseBlock):
    pass


class ConcreteBaseBlock(_BaseBlock):
    def __call__(self, **kwargs) -> "ConcreteBaseBlock":
        super().__call__(**kwargs)
        return self


# noinspection PyProtectedMember
@pytest.mark.parametrize("args", [(), ("test",)])
@pytest.mark.parametrize("klasse", [_BaseBlock, IncompleteConcreteBaseBlock])
def test_init_abstract(klasse, args):
    with pytest.raises(TypeError, match=r"Can't instantiate abstract class"):
        klasse(*args)


# noinspection PyArgumentList
def test_init():
    with pytest.raises(TypeError, match=r"missing \d+ required positional argument"):
        ConcreteBaseBlock()
    ConcreteBaseBlock("test")


@pytest.fixture()
def b() -> ConcreteBaseBlock:
    return ConcreteBaseBlock("test-block")


@pytest.fixture()
def b1() -> ConcreteBaseBlock:
    return ConcreteBaseBlock("test-block-1", "label1")


def test_call(b, b1, mockblock):
    b_called = b()
    assert isinstance(b_called, ConcreteBaseBlock)
    assert b_called is b

    kwargs = {"test_key": "test_value"}
    b_called = b(**kwargs)
    assert b_called is b
    _ = next(iter(b))
    mockblock.assert_called_with("test-block", kwargs)

    kwargs = {"test_key1": "test_value1"}
    b_called = b1(**kwargs)
    assert b_called is b1
    _ = next(iter(b1))
    mockblock.assert_called_with("test-block-1", "label1", kwargs)


def test_attrib(b):
    blk = next(iter(b))
    assert isinstance(blk, pretf.api.Block)
    assert str(blk) == "${test-block}"

    bdum = b.dummy
    assert isinstance(bdum, pretf.blocks.Interpolated)
    assert str(bdum) == "${test-block.dummy}"


# We cannot use mockblock here because depends_on.setter needs
# pretf.api's block() functionality.
def test_depends_on(b):
    c = ConcreteBaseBlock("dummy", "c")
    b.depends_on = c
    assert b.depends_on == ["dummy.c"]
    blk = next(iter(b))
    typ, cfg = next(iter(blk))
    assert typ == "test-block"
    assert cfg["depends_on"] == ["dummy.c"]

    d = ConcreteBaseBlock("dymmu", "d")
    b.depends_on = [c, d]
    assert b.depends_on == ["dummy.c", "dymmu.d"]
    blk = next(iter(b))
    typ, cfg = next(iter(blk))
    assert typ == "test-block"
    assert cfg["depends_on"] == ["dummy.c", "dymmu.d"]


class ConcreteProvider(_Provider):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __call__(self, **kwargs) -> "_Provider":
        return super().__call__(**kwargs)


# noinspection PyUnusedLocal
def dummy_renderer(*args, **kwargs):
    pass


def test_provider(b: ConcreteBaseBlock, testconn):
    marker: str = "test-provider"

    prov = ConcreteProvider(dummy_renderer, marker, test_connection=False)
    testconn.assert_not_called()

    b.provider = prov
    assert b.provider == marker

    b.provider = _ProviderAlias(marker)
    assert b.provider == marker

    b.provider = marker
    assert b.provider == marker

    with pytest.raises(TypeError):
        b.provider = None

    with pytest.raises(KeyError):
        b.provider = "Non-Existent-Provider"


def test_lifecycle(b: ConcreteBaseBlock, testconn):
    marker: str = "test-lifecycle"

    b.lifecycle = None

    with pytest.raises(TypeError):
        b.lifecycle = "some string"

    b.lifecycle = dict()
    assert b.lifecycle is None

    b.lifecycle = LifeCycle()

    with pytest.raises(TypeError):
        b.lifecycle = {"dummy": "dummy"}

    b.lifecycle = {"ignore_changes": "tags"}
    assert isinstance(b.lifecycle, LifeCycle)
    assert b.lifecycle["ignore_changes"] == ["tags"]

    b.lifecycle = LifeCycle(ignore_changes="tags")
    assert isinstance(b.lifecycle, LifeCycle)
    assert b.lifecycle["ignore_changes"] == ["tags"]
