import pytest
from unittest.mock import mock_open, MagicMock

import logging
from tempfile import NamedTemporaryFile
from textwrap import dedent
from functools import partial
from pathlib import Path

from pretf_helpers import ConfigClass, _log, AttribDict


mocked_file_contents = {
    "config.yaml": dedent(
        """\
        key1: val1
        key2:
          - val21
          - val22
          - val23
        key3: val3
        common: config.yaml
        """
    ),
    "secrets.yaml": dedent(
        """\
        sec1: sval1
        sec2: sval2
        common: secrets.yaml
        """
    ),
    "secrets2.yaml": dedent(
        """\
        sec21: sval21
        sec22: sval22
        common: secrets2.yaml
        """
    ),
    "one.yaml": dedent(
        """\
        myname: one.yaml
        """
    ),
    "two.yaml": dedent(
        """\
        myname: two.yaml
        """
    ),
    "config_notdict.yaml": dedent(
        """\
        - listmember1 : value1
        - listmember2 : value2
        """
    ),
    "config_tags1.yaml": dedent(
        """\
        default_tags:
            Name: AnyName
            Code: AnyCode
        """
    )
}


# Ref:
def mocked_file(fn, *args, **kwargs):
    fn = Path(fn).name
    if fn not in mocked_file_contents:
        return open(fn, *args, **kwargs)
    data = mocked_file_contents[fn]
    file_obj = mock_open(read_data=data).return_value
    file_obj.__iter__.return_value = data.splitlines(True)
    return file_obj


def assert_mockopened(m: MagicMock, fn: str):
    args, kwargs = m.call_args
    fpath, fmode = args
    assert Path(fpath).name == fn


@pytest.fixture()
def mocked_open(mocker):
    m = mocker.patch("builtins.open", side_effect=mocked_file)
    m.assert_opened = partial(assert_mockopened, m)
    return m


# noinspection PyArgumentList
def test_init(mocked_open):
    with pytest.raises(
        TypeError, match=r"takes \d+ positional argument but \d+ were given"
    ):
        ConfigClass("one")

    c1 = ConfigClass()
    mocked_open.assert_not_called()

    c2 = ConfigClass()
    mocked_open.assert_not_called()
    assert c1 == c2
    assert c1 is c2
    mocked_open.assert_not_called()

    c3 = ConfigClass(config_file="different_file.yaml")
    mocked_open.assert_not_called()
    assert c3 != c1
    assert c3 is not c1
    mocked_open.assert_not_called()


def test_filenotfound():
    # Ensure we have a filename that does NOT exist
    with NamedTemporaryFile() as tmpf:
        fn = tmpf.name
        # Upon exiting the context manager, NamedTemporaryFile guarantees
        # the tempfile to be deleted

    c4 = ConfigClass(config_file=fn)
    with pytest.raises(_log.bad, match=r"Cannot find config file "):
        _ = c4.any_key

    c5 = ConfigClass(secrets_file=fn)
    assert c5 != c4
    assert c5 is not c4
    with pytest.raises(_log.bad, match=r"Cannot find secrets file "):
        _ = c4.SECRETS.any_key


def test_filecontentserror(mocked_open):
    c6 = ConfigClass(config_file="config_notdict.yaml")
    mocked_open.assert_not_called()
    with pytest.raises(_log.bad, match=r"should have been a dict, but it's not"):
        _ = c6.any_key
    mocked_open.assert_called_once()


@pytest.mark.parametrize("filename", ["one.yaml", "two.yaml"])
@pytest.mark.parametrize("kwarg", ["config_file", "secrets_file"])
def test_readfile(mocked_open, kwarg, filename):
    cc = ConfigClass(**{kwarg: filename})
    mocked_open.assert_not_called()
    if kwarg == "config_file":
        assert cc.myname == filename
        assert cc["myname"] == filename
    else:
        assert cc.SECRETS.myname == filename
        assert cc.SECRETS["myname"] == filename
    mocked_open.assert_called_once()
    mocked_open.assert_opened(filename)


def test_getattr(mocked_open):
    cc = ConfigClass()
    mocked_open.assert_not_called()

    assert cc.key1 == "val1"
    mocked_open.assert_called_once()
    mocked_open.assert_opened("config.yaml")

    assert isinstance(cc.key2, list)
    mocked_open.assert_called_once()

    assert cc.key3 == "val3"
    mocked_open.assert_called_once()

    with pytest.raises(AttributeError):
        _ = cc.key_not_exist


def test_SECRETS(mocked_open):
    cc = ConfigClass()
    mocked_open.assert_not_called()

    assert cc.SECRETS.sec1 == "sval1"
    mocked_open.assert_called_once()
    mocked_open.assert_opened("secrets.yaml")

    assert cc.SECRETS["sec2"] == "sval2"
    mocked_open.assert_called_once()

    with pytest.raises(AttributeError):
        _ = cc.SECRETS.key_not_exist


def test_SECRETS_fromstr(mocked_open, caplog):
    cc = ConfigClass()
    mocked_open.assert_not_called()

    cc.SECRETS = "seckey: secval"
    mocked_open.assert_not_called()
    assert cc.SECRETS.seckey == "secval"

    mocked_open.reset_mock()
    caplog.set_level(logging.CRITICAL)

    cc.SECRETS = "- elem1: a\n- elem2: b"
    mocked_open.assert_not_called()
    with pytest.raises(_log.bad, match=r"Secrets file [\s\S]+ should have been a dict"):
        _ = cc.SECRETS.seckeyfail


def test_SECRETS_fromfile(mocked_open, mocker):
    cc = ConfigClass()
    mocked_open.assert_not_called()

    filename = "secrets2.yaml"
    mocker.patch("pretf_helpers.utils.Path.exists", return_value=True)
    cc.SECRETS = filename
    assert cc.SECRETS.sec21 == "sval21"
    mocked_open.assert_called_once()
    mocked_open.assert_opened(filename)


def test_tags(mocked_open):
    cfg = "config_tags1.yaml"
    cc = ConfigClass(config_file=cfg)
    mocked_open.assert_not_called()

    tags = cc.tags()
    mocked_open.assert_called_once()
    mocked_open.assert_opened(cfg)
    assert isinstance(tags, dict)
    assert "Name" in tags
    assert "Code" in tags
    assert tags["Name"] == "AnyName"
    assert tags["Code"] == "AnyCode"

    tags = cc.tags(Name="NewName")
    mocked_open.assert_called_once()
    assert isinstance(tags, dict)
    assert "Name" in tags
    assert "Code" in tags
    assert tags["Name"] == "NewName"
    assert tags["Code"] == "AnyCode"

    tags = cc.tags(Code=None)
    mocked_open.assert_called_once()
    assert isinstance(tags, dict)
    assert "Name" in tags
    assert "Code" not in tags
    assert tags["Name"] == "AnyName"


def test_fromitem(mocked_open):
    cc = ConfigClass()
    mocked_open.assert_not_called()

    with pytest.raises(TypeError):
        cc.from_item_(None)

    item = {"key1": "val1"}
    rslt = cc.from_item_(item)
    assert isinstance(rslt, AttribDict)
    assert rslt == item

    item = rslt
    rslt = cc.from_item_(item)
    assert isinstance(rslt, AttribDict)
    assert rslt is not item

    rslt = cc.from_item_("key3")
    assert rslt == "val3"

    with pytest.raises(KeyError):
        _ = cc.from_item_("key_not_exist")
