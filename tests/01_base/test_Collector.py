import pytest
from unittest.mock import MagicMock

from functools import partial

# noinspection PyProtectedMember
from pretf_helpers import _Collector, LabelConflict


class ConcreteCollector(_Collector):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


def dummy_null():
    return


# noinspection PyUnusedLocal
def dummy_2args(one, two):
    return


def test_init(abstract_exception, missing_args_exception):
    for args in ((), (None, )):
        with abstract_exception:
            _Collector(*args)

    with missing_args_exception:
        ConcreteCollector()

    with pytest.raises(TypeError):
        ConcreteCollector(None)

    ConcreteCollector(dummy_null)
    ConcreteCollector(partial(dummy_2args, "dummy"))


def test_setitem():
    c1 = ConcreteCollector(dummy_null)

    with pytest.raises(ValueError):
        # noinspection PyTypeChecker
        c1["test-item"] = "test-item-value"

    data = {"a": "aa", "b": "bb"}
    key = "test-item"
    c1[key] = data

    with pytest.raises(LabelConflict):
        c1[key] = data

    assert c1.get_raw(key) == data


def test_getitem():
    dumper = MagicMock()
    renderer = MagicMock(return_value=dumper)
    c1 = ConcreteCollector(renderer)
    data = {"a": "aa", "b": "bb"}
    key = "test-item"
    c1[key] = data
    _ = c1[key]
    renderer.assert_called_once()
    renderer.assert_called_with(key)
    dumper.assert_called_once()
    dumper.assert_called_with(**data)


def test_element(mocker):
    c1 = ConcreteCollector(dummy_null)
    espy = mocker.spy(c1, "_element")
    data = {"a": "aa", "b": "bb"}
    key = "test-item"
    c1[key] = data
    espy.assert_called_once()
    espy.assert_called_with(data)


def test_iter():
    def back_atcha(**kwargs):
        return kwargs
    dumper = MagicMock(side_effect=back_atcha)
    renderer = MagicMock(return_value=dumper)
    c1 = ConcreteCollector(renderer)
    data = {
        "item1": {"a1": "aa", "b1": "bb"},
        "item2": {"a2": "cc", "b2": "dd"},
    }
    for k, v in data.items():
        c1[k] = v
    for c, i in enumerate(c1, start=1):
        assert isinstance(i, dict)
        assert i in data.values()
        assert renderer.call_count == c
        assert dumper.call_count == c
