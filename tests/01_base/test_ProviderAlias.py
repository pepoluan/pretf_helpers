# noinspection PyUnresolvedReferences
import pytest

# noinspection PyProtectedMember
from pretf_helpers import _ProviderAlias


# noinspection PyArgumentList
def test_init(missing_args_exception):
    with missing_args_exception:
        _ProviderAlias()

    pa1 = _ProviderAlias("test1")
    assert pa1.provider == "test1"
    assert pa1.alias is None
    assert str(pa1) == "test1"
    assert repr(pa1) == "_ProviderAlias(provider='test1', alias=None)"
    assert pa1 == ("test1", None)

    pa2 = _ProviderAlias("test2", "dummy")
    assert pa2.provider == "test2"
    assert pa2.alias == "dummy"
    assert str(pa2) == "test2.dummy"
    assert repr(pa2) == "_ProviderAlias(provider='test2', alias='dummy')"
    assert pa2 == ("test2", "dummy")
    assert pa2 != pa1

    pa3 = _ProviderAlias.parse("test3")
    assert pa3.provider == "test3"
    assert pa3.alias is None
    assert str(pa3) == "test3"
    assert pa3 == ("test3", None)

    pa4 = _ProviderAlias.parse("test4.du")
    assert pa4.provider == "test4"
    assert pa4.alias == "du"
    assert str(pa4) == "test4.du"
    assert pa4 == ("test4", "du")
    assert pa4 != pa3

    pa5 = _ProviderAlias("test4", "du")
    assert pa5 == pa4
    assert pa5 is not pa4
