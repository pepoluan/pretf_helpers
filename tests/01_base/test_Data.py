# noinspection PyProtectedMember
from pretf_helpers import Data, _BaseBlock


# noinspection PyArgumentList
def test_init_fail(missing_args_exception):
    with missing_args_exception:
        Data()
    with missing_args_exception:
        Data("dummy")


def test_init_success():
    Data("dummy", "dummylabel")


def test_call(mocker):
    basecall = mocker.spy(_BaseBlock, "__call__")
    r = Data("dummy", "dummylabel")
    basecall.assert_not_called()
    data = {"k1": "v1", "k2": "v2"}
    r(**data)
    basecall.assert_called_once()
    # Need to include "r" in front, because first arg of __call__ is "self"
    basecall.assert_called_with(r, **data)
