import pytest

# noinspection PyProtectedMember
from pretf_helpers import _Backend


# noinspection PyAbstractClass
class IncompleteConcreteBackend(_Backend):
    pass


class ConcreteBackend(_Backend):
    @classmethod
    def from_config_(cls, config):
        return super().from_config_(config)

    @property
    def type(self) -> str:
        return "test-backend"

    @property
    def remote_config(self) -> dict:
        return self._cfg

    ConfigAttribRemaps = {"alias1": "actual1", "alias2": "actual2"}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._renderer = dict


@pytest.mark.parametrize("klasse", [_Backend, IncompleteConcreteBackend])
def test_init_fail(abstract_exception, klasse):
    with abstract_exception:
        klasse()


def test_init_success(testconn):
    ConcreteBackend()
    testconn.assert_not_called()
    ConcreteBackend(test_connection=True)
    testconn.assert_called_once()


def test_fromconfig(testconn):
    be = ConcreteBackend.from_config_({"arg1": 11, "arg2": 12, "alias1": 13})
    testconn.assert_not_called()
    assert be.type == "test-backend"
    assert "arg1" in be.remote_config
    assert "arg2" in be.remote_config
    assert "alias1" not in be.remote_config
    c = None
    for c, o in enumerate(be, start=1):
        assert isinstance(o, dict)
    assert c == 1
