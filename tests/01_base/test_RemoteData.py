import pytest

# noinspection PyProtectedMember
from pretf_helpers import Data, RemoteData, _Backend

# noinspection PyProtectedMember
from pretf.blocks import Interpolated


class ConcreteBackend(_Backend):
    @classmethod
    def ConfigAttribRemaps(cls) -> dict:
        return {}

    @property
    def type(self) -> str:
        return "dymmy"

    @property
    def remote_config(self) -> dict:
        return {
            "cfg1": "11",
            "cfg2": "22",
        }


# noinspection PyArgumentList
def test_init(missing_args_exception):
    with missing_args_exception:
        RemoteData()
    RemoteData("dummy1")


# noinspection PyArgumentList
def test_call_fail(missing_args_exception):
    rd = RemoteData("dummy2")
    with missing_args_exception:
        rd()
    with missing_args_exception:
        rd(data="dummy")
    with pytest.raises(ValueError):
        rd(backend=ConcreteBackend(), data="dummy")


def test_call_success(mocker):
    rd = RemoteData("dummy2")
    datacall = mocker.spy(Data, "__call__")

    backend = ConcreteBackend()
    rdc = rd(backend=backend)
    assert rdc == rd
    datacall.assert_called_once()
    datacall.assert_called_with(rd, backend=backend.type, config=backend.remote_config)

    datacall.reset_mock()

    cfg = {"cfg3": "ccc", "cfg4": "dddd"}
    rdc = rd(backend="dumdum", **cfg)
    assert rdc == rd
    datacall.assert_called_once()
    datacall.assert_called_with(rd, backend="dumdum", config=cfg)


# noinspection PyStatementEffect
def test_metaget():
    RemoteData["dummy1"]
    RemoteData["dummy2"]
    RemoteData["dummy3"]

    RemoteData.dummy1
    RemoteData.dummy2
    RemoteData.dummy3


def test_outputs():
    rd = RemoteData("test-output")
    rd(backend="test-out", cfg7=77, cfg8=88)

    blk = next(iter(rd))
    assert str(blk) == "${data.terraform_remote_state.test-output}"

    o1 = rd["o1"]
    assert isinstance(o1, Interpolated)
    assert str(o1) == "${data.terraform_remote_state.test-output.outputs.o1}"

    o2 = rd.o2
    assert isinstance(o2, Interpolated)
    assert str(o2) == "${data.terraform_remote_state.test-output.outputs.o2}"


def test_iter(mockblock):
    marker = "dim1"
    cfg = {"cfg5": 55, "cfg6": 66}
    RemoteData["dummy1"](backend=marker, **cfg)
    blk = next(iter(RemoteData["dummy1"]))
    mockblock.assert_called_once()
    struct = {"backend": marker, "config": cfg}
    mockblock.assert_called_with("data", "terraform_remote_state", "dummy1", struct)
    assert str(blk) == "${data.terraform_remote_state.dummy1}"
