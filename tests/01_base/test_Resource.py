# noinspection PyProtectedMember
from pretf_helpers import Resource, _BaseBlock


# noinspection PyArgumentList
def test_init(missing_args_exception):
    with missing_args_exception:
        Resource()
    with missing_args_exception:
        Resource("dummy")

    Resource("dummy", "dummylabel")


def test_call(mocker):
    basecall = mocker.spy(_BaseBlock, "__call__")
    r = Resource("dummy", "dummylabel")
    basecall.assert_not_called()
    data = {"k1": "v1", "k2": "v2"}
    r(**data)
    basecall.assert_called_once()
    # Need to include "r" in front, because first arg of __call__ is "self"
    basecall.assert_called_with(r, **data)
