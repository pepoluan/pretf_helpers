import pytest

# noinspection PyProtectedMember
from pretf_helpers import _Provider, _ProviderAlias


# noinspection PyUnusedLocal
def dummy_renderer(*args, **kwargs):
    pass


# noinspection PyAbstractClass
class IncompleteConcreteProvider(_Provider):
    pass


class ConcreteProvider(_Provider):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __call__(self, **kwargs) -> "_Provider":
        return super().__call__(**kwargs)


# noinspection PyArgumentList
def test_init_exceptions(abstract_exception, missing_args_exception):
    with abstract_exception:
        _Provider()
    with abstract_exception:
        IncompleteConcreteProvider()

    with missing_args_exception:
        ConcreteProvider()
    with missing_args_exception:
        ConcreteProvider(dummy_renderer)


def test_init_success(testconn):
    marker = "test-init"
    cp = ConcreteProvider(dummy_renderer, marker)
    testconn.assert_called_once()

    pa = _ProviderAlias(marker)
    assert isinstance(cp.id, _ProviderAlias)
    assert cp.id == pa
    assert cp.id is not pa
    assert cp.id == (marker, None)


# noinspection PyProtectedMember
def test_contains():
    marker = "test-contains"
    ConcreteProvider(dummy_renderer, marker, test_connection=False)
    pa = _ProviderAlias(marker)
    assert pa in _Provider
    assert marker in _Provider
    with pytest.raises(KeyError):
        ConcreteProvider(dummy_renderer, marker)


# noinspection PyTypeChecker
@pytest.mark.parametrize("value", [None, 1])
def test_contains_fail(value):
    with pytest.raises(TypeError):
        _ = value in _Provider


def passthru_renderer(**kwargs) -> dict:
    return kwargs


# noinspection PyUnusedLocal
def test_call_iter(testconn):
    marker = "test-call"
    cp = ConcreteProvider(passthru_renderer, marker)
    cfg = {"arg1": 1, "arg2": 2}
    assert cp(**cfg) is cp
    body = next(iter(cp))
    assert isinstance(body, dict)
    assert body == cfg


# noinspection PyUnusedLocal
def test_reference(testconn):
    marker = "test-ref"
    cp = ConcreteProvider(dummy_renderer, marker)
    assert cp.reference == marker
