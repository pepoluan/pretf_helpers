# noinspection PyProtectedMember
from pretf_helpers import _CompoundResource, _BaseBlock, _Collector


class ConcreteCollector(_Collector):
    def __init__(self):
        super().__init__(self.renderer)

    def renderer(self, _):
        return self.dumper

    @staticmethod
    def dumper(**kwargs):
        return kwargs


# noinspection PyAbstractClass
class IncompleteConcreteCompoundResource(_CompoundResource):
    pass


class ConcreteCompoundResource(_CompoundResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._subres = ConcreteCollector()

    def __call__(self, **kwargs) -> "_CompoundResource":
        return super().__call__(**kwargs)


# noinspection PyArgumentList
def test_init_fail(abstract_exception, missing_args_exception):
    with abstract_exception:
        _CompoundResource()
    with abstract_exception:
        IncompleteConcreteCompoundResource()

    with missing_args_exception:
        ConcreteCompoundResource()
    with missing_args_exception:
        ConcreteCompoundResource("dummy")


def test_init_success():
    ConcreteCompoundResource("dummy1", "dummy2")


def test_call(mocker):
    basecall = mocker.spy(_BaseBlock, "__call__")
    cr = ConcreteCompoundResource("dummy", "dummylabel")
    basecall.assert_not_called()
    data = {"k1": "v1", "k2": "v2"}
    cr(**data)
    basecall.assert_called_once()
    # Need to include "r" in front, because first arg of __call__ is "self"
    basecall.assert_called_with(cr, **data)


def test_setgetitem():
    cr = ConcreteCompoundResource("dummy", "dummylabel")
    key = "testkey"
    val = {"a": 11, "b": 22}
    cr[key] = val
    assert cr[key] == val


def test_iter(mockblock):
    cr = ConcreteCompoundResource("dummy", "dummylabel")
    cfg = {"k1": "v1", "k2": "v2"}
    cr(**cfg)
    data = {
        i: {"1": i, "2": i*2} for i in "abc"
    }
    for k, v in data.items():
        cr[k] = v
    for c, i in enumerate(cr):
        if c == 0:
            mockblock.assert_called_once()
            mockblock.assert_called_with("resource", "dummy", "dummylabel", cfg)
        else:
            assert isinstance(i, dict)
            assert i in data.values()
