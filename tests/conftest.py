import pytest
from unittest.mock import MagicMock


import pretf_helpers


@pytest.fixture()
def mockblock(mocker):
    """Mock pretf's block() function"""
    return mocker.spy(pretf_helpers, "block")


@pytest.fixture()
def testconn(mocker) -> MagicMock:
    """Mock pretf-helpers' _test_connection() function"""
    return mocker.patch("pretf_helpers._test_connection")


@pytest.fixture()
def missing_args_exception():
    return pytest.raises(TypeError, match=r"missing \d+ required positional argument")


@pytest.fixture()
def abstract_exception():
    return pytest.raises(TypeError, match=r"Can't instantiate abstract class")


@pytest.fixture()
def argcount_exception():
    return pytest.raises(
        TypeError, match=r"takes \d+ positional argument but \d+ were given"
    )


# Ref: https://docs.pytest.org/en/latest/example/simple.html#control-skipping-of-tests-according-to-command-line-option

def pytest_addoption(parser):
    parser.addoption(
        "--runslow", action="store_true", default=False, help="run slow tests"
    )


def pytest_configure(config):
    config.addinivalue_line("markers", "slow: mark test as slow to run")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--runslow"):
        # --runslow given in cli: do not skip slow tests
        return
    skip_slow = pytest.mark.skip(reason="need --runslow option to run")
    for item in items:
        if "slow" in item.keywords:
            item.add_marker(skip_slow)
