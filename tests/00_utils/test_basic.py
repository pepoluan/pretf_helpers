# noinspection PyUnresolvedReferences
import pytest

from urllib.request import urlopen
from urllib.error import HTTPError, URLError
from socket import timeout as sock_timeout

from pretf_helpers import unwrap, InitScript, AttribDict, TemplateDecoder
from pretf_helpers.utils import _test_connection


def test_unwrap():
    assert unwrap("1") == "1"
    assert unwrap("${1}") == "1"
    assert unwrap("${1") == "${1"
    assert unwrap("1}") == "1}"


expected = """\
#!/bin/bash
sudo hostnamectl set-hostname test-name
printf "\\n%s\\n" "test-key-1" >> /home/test-user-a/.ssh/authorized_keys
printf "\\n%s\\n" "test-key-2" >> /home/test-user-a/.ssh/authorized_keys
chown test-user-a:test-user-a /home/test-user-a/.ssh/authorized_keys
chmod -R 0600 /home/test-user-a/.ssh /home/test-user-a/.ssh/*
printf "\\n%s\\n" "test-key-3" >> /home/test-user-b/.ssh/authorized_keys
chown test-user-b:test-user-b /home/test-user-b/.ssh/authorized_keys
chmod -R 0600 /home/test-user-b/.ssh /home/test-user-b/.ssh/*
"""


def test_InitScript():
    i = InitScript(
        "test-name",
        {"test-user-a": ["test-key-1", "test-key-2"], "test-user-b": ["test-key-3"]},
    )
    assert i.generate() == expected


def test_AttribDict():
    assert issubclass(AttribDict, dict)

    ad = AttribDict()
    assert isinstance(ad, dict)
    ad["teskey"] = "tesval"
    assert ad.teskey == "tesval"

    d = {
        "k1": "v1",
        "k2": {"k21": "v21", "k22": {"k221": "v221"}, "k23": "v23"},
        "k3-dashed": "v3",
    }
    ad = AttribDict.recursively_from_dict_(d)
    assert isinstance(ad.k2, AttribDict)
    assert isinstance(ad.k2.k22, AttribDict)
    assert ad.k2.k22.k221 == "v221"
    assert ad.k3_dashed == "v3"


# noinspection PyTypeChecker
@pytest.mark.parametrize("value", [None, 1, "string"])
def test_AttribDict_fail(value):
    with pytest.raises(TypeError):
        AttribDict.recursively_from_dict_(None)


@pytest.fixture()
def cheko(mocker):
    # noinspection PyUnusedLocal
    def bycmd(cmd, *args, **kwargs) -> bytes:
        assert isinstance(cmd, list)
        if cmd == ["whoami"]:
            return "test-user".encode()
        if cmd[0:3] == "git remote get-url".split():
            return "test-repo".encode()

    return mocker.patch("pretf_helpers.utils.check_output", side_effect=bycmd)


def test_TemplateDecoder(cheko):
    tdne = TemplateDecoder(error_on_unknown=False)
    assert tdne.render("{UNKNOWN}") == "{UNKNOWN}"

    tde = TemplateDecoder(error_on_unknown=True)
    with pytest.raises(KeyError, match=r"Unknown Template Key"):
        tde.render("{UNKNOWN}")


def test_TemplateDecoder_var(cheko, freezer):
    tde = TemplateDecoder(error_on_unknown=True)

    assert tde.render("{CURRENT_USER}") == "test-user"
    cheko.assert_called_once()

    assert tde.render("{CURRENT_USER}") == "test-user"
    assert cheko.call_count == 1

    assert tde.render("{CURRENT_REPO}") == "test-repo"
    assert cheko.call_count == 2

    assert tde.render("{CURRENT_REPO}") == "test-repo"
    assert cheko.call_count == 2

    freezer.move_to("2021-12-31 23:59:58")
    assert tde.render("{CURRENT_DATETIME}") == "20211231235958"
    freezer.tick()
    assert tde.render("{CURRENT_DATETIME}") == "20211231235959"


@pytest.mark.slow
def test_TemplateDecoder_var_slow():
    tde = TemplateDecoder(error_on_unknown=True)

    with urlopen("https://ipecho.net/plain") as http:
        ip_ipecho = http.read().decode()
    with urlopen("https://api.ipify.org/?format=raw") as http:
        ip_ipify = http.read().decode()
    assert tde.render("{CURRENT_MY_PUBLIC_IP}") == ip_ipecho
    assert tde.render("{CURRENT_MY_PUBLIC_IP}") == ip_ipify


def test_testconn(mocker):
    lw = mocker.patch("pretf_helpers.utils.log.warn")
    muo = mocker.patch("pretf_helpers.utils.urlopen", autospec=True)

    m = mocker.Mock()
    muo.side_effect = HTTPError(m, m, m, m, m)
    _test_connection()
    lw.assert_called_once()
    lw.reset_mock()

    muo.side_effect = URLError("dummy")
    _test_connection()
    lw.assert_called_once()
    lw.reset_mock()

    muo.side_effect = sock_timeout()
    _test_connection()
    lw.assert_called_once()
    lw.reset_mock()
